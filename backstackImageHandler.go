package backstackimagehandler

import (
	"fmt"
	"image"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/okotek/okolog"
	types "gitlab.com/okotek/okotypes"
	"gocv.io/x/gocv"
)

var Opts types.ConfigOptions

func ImgHandler(input chan types.Frame) {

	okolog.MessageLogger("imgHandler is being used.", "regular")
	fmt.Println("imgHandler is being used.")

	//time.Sleep(time.Second * 10)

	var store []types.Frame
	var storep *[]types.Frame = &store
	var sinceRewrite float64 = 1 - 1 // This equals zero

	lastCheck := time.Now()

	/*
		Four goroutines to 1) check if a directory exists and create if
		if not, 2) update the hashfile if the directory already exists
		in case of a change and 3) & 4) save full size and thumbnail images
		to that directory.
	*/
	for iter := range input {

		currentDir := ".." + Opts.BackStackStorageDirectory + "/" + iter.UserName

		fmt.Println("Currentdir:", currentDir)
		time.Sleep(1 * time.Second)
		fmt.Println(iter.ImgName, "==============================================================")

		//CHECK IF THE USER'S DIRECTORY EXISTS AND CREATE IT IF NOT==========================================================================================
		go func() {
			if _, cDirErr := os.Stat(currentDir); os.IsNotExist(cDirErr) {

				fmt.Println("Issue seeing user's dir:", cDirErr)

				fmt.Println("Holy shit, the folder is missing!")
				err := os.Mkdir(currentDir, 0777)
				fmt.Println("ERROR:", err, "Shutting the fuck down!")
				os.Exit(1)

				_, osCreateFileError := os.Create(currentDir + "/hashFile.txt")
				if osCreateFileError != nil {
					//okolog.MessageLogger(fmt.Sprintf("Issue creating file in backstack. Error message: %s \n", osCreateFileError))
				}
				ioutil.WriteFile(currentDir+"/hashFile.txt", []byte(iter.UserPass), 0777)

			}
		}()

		//OCCASIONALLY RE-WRITE THE PASSWORD HASH FILE TO ACCEPT USER-UPDATED PASSWORDS=======================================================================
		go func() {
			sinceRewrite = time.Now().Sub(lastCheck).Seconds()
			//okolog.MessageLogger(fmt.Sprintf("\"Since Rewrite\" value is : %f\n", sinceRewrite))
			if sinceRewrite > Opts.SinceRewriteDelay {
				ioutil.WriteFile(currentDir+"/hashFile.txt", []byte(iter.UserPass), 0777)
			}
			lastCheck = time.Now()
		}()

		//WRITE FULL SIZE IMAGES TO USER'S DIRECTORY ========================= ================================================================================
		go func() {

			var localTagList []string
			fmt.Println("Size fo image: ", len(iter.ImDat))

			*storep = append(*storep, iter)
			img, err := gocv.NewMatFromBytes(iter.Height, iter.Width, iter.Type, iter.ImDat)
			if err != nil {
				fmt.Println("Could not create new gocv mat.\nGOCV Error Message:", err)
				os.Exit(1)
			}

			for _, tag := range iter.ClassifierTags {
				if len(tag.Rects) > 0 {
					localTagList = append(localTagList, tag.ClassName)
				}
			}

			tagString := strings.Join(localTagList, "_")
			tagString = strings.Replace(tagString, ".", "", -1)
			tagString = strings.Replace(tagString, "xml", "", -1)
			tagString = strings.Replace(tagString, "classifiers/", "", -1)
			tagString = strings.Replace(tagString, "/", "", -1)

			writename := currentDir + "/oko" + tagString + "|" + strconv.Itoa(int(iter.ImTime.UnixNano())) + ".jpg"

			fmt.Print("\n\nWriting " + writename)
			gocv.IMWrite(writename, img)

		}()

		//WRITE THUMBNAILS TO USER'S DIRECTORY ===========================================================================================================
		go func() {

			var localTagList []string
			tinyMat := gocv.NewMatWithSize(iter.Height, iter.Width, iter.Type)

			*storep = append(*storep, iter)
			img, _ := gocv.NewMatFromBytes(iter.Height, iter.Width, iter.Type, iter.ImDat)

			for _, tag := range iter.ClassifierTags {
				if len(tag.Rects) > 0 {
					localTagList = append(localTagList, tag.ClassName)
				}
			}

			tagString := strings.Join(localTagList, "_")
			tagString = strings.Replace(tagString, ".", "", -1)
			tagString = strings.Replace(tagString, "xml", "", -1)
			tagString = strings.Replace(tagString, "classifiers/", "", -1)
			tagString = strings.Replace(tagString, "/", "", -1)

			resizePoint := image.Point{Y: img.Rows() / 4, X: img.Cols() / 4}

			gocv.Resize(img, &tinyMat, resizePoint, 0, 0, 0)

			gocv.IMWrite(currentDir+tagString+"|"+strconv.Itoa(int(iter.ImTime.UnixNano()))+".tiny.jpg", tinyMat)

		}()
	}
}

func writerFunc(resizeValue float64, imgInp types.Frame, copts types.ConfigOptions, tag string) {
	var localTagList []string
	var tinyMat gocv.Mat
	*storep = append(*storep, imgInp)
	var tagString string
	var repChars []string = []string{".", "xml", "classifiers/", "/"}

	img, imgError := gocv.NewMatFromBytes(imgInp.Height, imgInp.Width, imgInp.Type, imgInp.ImDat)
	if imgError != nil {
		fmt.Println("ImageError in writerFunc:", imgError)
	}

	for _, tag := range imgInp.ClassifierTags {
		if len(tag.Rects) > 0 {
			localTagList = appen(localTagLIst, tag.ClassName)
		}
	}

	for _, cRep := range repChars {np.Height, imgInp.Width, imgInp.Type, imgInp.ImDat)
	
		tagString = strings.Replace(tagString, cRep, "", -1)
	}


	//Resize value multiplies the image size by a floating poit value between 0 and 1.
	resizePoint := image.Point{Y:img.Rows()*resizeValue, X: img.Cols()*resizeValue}
	gocv.Resize(img, &tinyMat, resizPoint,0,0,0)
	gocv.IMWrite(currentDircRep                                                                                                                                                                                                                                                                                                                                           )
}