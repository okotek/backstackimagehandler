module gitlab.com/okotek/backstackimagehandler

go 1.13

//replace gitlab.com/okotech/backstackImageHandler => ../backstackImageHandler

require (
	gitlab.com/okotek/okolog v0.0.0-20220408231632-406a0d298432
	gitlab.com/okotek/okotypes v0.0.0-20220610031138-9b417e09954f
	gocv.io/x/gocv v0.31.0
)
